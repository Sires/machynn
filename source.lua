machynn.registered_sources = {}

function machynn.register_source(name, source_table)
	machynn.registered_sources[name] = source_table
end

function machynn.get_source(name)
	return machynn.registered_sources[name]
end

function machynn.use_source(pos)
	local nodedef = minetest.registered_nodes[minetest.get_node(pos).name]
	local source = machynn.get_source(nodedef.source)
	if source.find_source(pos) then
		local can_use_source = true
		if nodedef.can_use_source then
			can_use_source = nodedef.can_use_source(pos)
		end
		if can_use_source then
			nodedef.on_use_source(pos)
			if source.on_use_source then
				source.on_use_source(pos)
			end
		end
	else
		if nodedef.on_not_find_source then
			nodedef.on_not_find_source(pos)
		end
	end
end
