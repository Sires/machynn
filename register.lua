machynn.on_place_nodelist = {}
machynn.on_dig_nodelist = {}

function machynn.register_nodes(nodedefs)
	for name, def in pairs(nodedefs) do
		machynn.register_node(name, def)
	end
end

function machynn.register_node(name, nodedef)
	minetest.register_node(name, nodedef)
	machynn.on_place_nodelist[name] = def.source
	machynn.on_dig_nodelist[name] = def.source
end

minetest.register_on_placenode(function(pos, newnode, placer, oldnode, itemstack, pointed_thing)
	if machynn.on_place_nodelist[newnode.name] then
		local source = machynn.get_source(machynn.on_place_nodelist[newnode.name])
		if source.on_place_node then
			source.on_place_node(pos, newnode, placer, oldnode, itemstack, pointed_thing)
		end
	end
end)

minetest.register_on_dignode(function(pos, oldnode, digger)
	if machynn.on_dig_nodelist[oldnode.name] then
		local source = machynn.get_source(machynn.on_dig_nodelist[oldnode.name])
		if source.on_dig_node then
			source.on_dig_node(pos, oldnode, digger)
		end
	end
end)
